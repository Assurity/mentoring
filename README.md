# README #

a start project for assurity mentoring application.

## What we have now:
- SpringBoot for web service
- Google App Engine for running this web service (current URL: https://mentoringtest001.appspot-preview.com/)
- A RESTful sample using Angular1 to do 'Get' request
- A 'Hello World' sample using Angular2

## How to startup/run:

### Prerequisites
- JDK 1.8 (required)
- Google App Engine Java (_optional for now_) 

### Run in local
command line:

    gradlew bootRun

- access http://localhost:8080 for Hello World
- access http://localhost:8080/greeting for RESTful sample, currently it's returning a JSON string


### Run in Google App Engine:

Note: this is google account related and it needs local Google Could SDK installed before deploying.

command line:
    
    gradlew appengineDeploy
    