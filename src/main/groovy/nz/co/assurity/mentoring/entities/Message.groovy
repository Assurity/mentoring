package nz.co.assurity.mentoring.entities

/**
 * Created by kris on 27/01/17.
 */
class Message {

    private String message;

    public Message(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
