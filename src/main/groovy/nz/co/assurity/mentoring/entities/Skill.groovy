package nz.co.assurity.mentoring.entities

import com.google.cloud.datastore.Entity

/**
 * Created by kris on 26/01/17.
 */
class Skill {

    def id
    def name
    def categoryID

    Skill(Entity entity) {
        this.id = entity.getString('id')
        this.name = entity.getString('name')
        this.categoryID = entity.getString('categoryID')
    }

    Skill() {

    }
}
