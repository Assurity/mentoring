package nz.co.assurity.mentoring.entities

import com.google.cloud.datastore.FullEntity

/**
 * Created by kris on 28/01/17.
 */
class Category {

    def name
    def description

    Category(FullEntity entity) {
        this.name = entity.getString('name')
        this.description = entity.getString('description')
    }

    Category() {

    }
}
