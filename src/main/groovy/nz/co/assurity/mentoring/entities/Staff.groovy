package nz.co.assurity.mentoring.entities

import com.google.cloud.datastore.Entity
import com.google.cloud.datastore.StringValue


/**
 * Created by kris on 27/01/17.
 */
class Staff {

    def id
    def name
    def email
    List<String> mentorSkills = []
    List<String> menteeSkills = []

    Staff(Entity staff) {
        this.id = staff.getKey().nameOrId.toString()
        this.name = staff.getString('name')
        this.email = staff.getString('email')
        List<StringValue> mentorSkillListString = staff.getList('mentorSkills')
        List<StringValue> menteeSkillListString = staff.getList('menteeSkills')

        for(item in mentorSkillListString) {
            this.mentorSkills.add(item.get())
        }

        for(item in menteeSkillListString) {
            this.menteeSkills.add(item.get())
        }

//        List<EntityValue> mentorSkillListEntity = staff.getList('mentorSkills')
//        Skill mentorSkill
//        mentorSkillListEntity.each { entity ->
//            mentorSkill = new Skill(entity)
//            this.mentorSkills.add(mentorSkill)
//        }
//        List<EntityValue> menteeSkillListEntity = staff.getList('menteeSkills')
//        Skill menteeSkill
//        menteeSkillListEntity.each { entity ->
//            menteeSkill = new Skill(entity)
//            this.menteeSkills.add(menteeSkill)
//        }
    }

    Staff() {

    }
}
