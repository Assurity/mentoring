package nz.co.assurity.mentoring.entities

import com.google.cloud.datastore.Entity

/**
 * Created by Di on 14/02/17.
 */
class Match {

    def mentor
    def mentee
    def skill

    Match(Entity entity) {
        this.mentor = entity.getString('mentor')
        this.mentee = entity.getString('mentee')
        this.skill = entity.getString('skill')
    }

    Match() {

    }
}
