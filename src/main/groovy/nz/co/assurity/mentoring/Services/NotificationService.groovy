package nz.co.assurity.mentoring.Services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mail.MailException
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessagePreparator
import org.springframework.stereotype.Service

import javax.mail.Message
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

@Service
class NotificationService {

    @Autowired
    JavaMailSender javaMailSender

    def sendNotification(String mentor, String mentee, String body) throws MailException {

        // send email
//        SimpleMailMessage mail = new SimpleMailMessage()
//        mail.setTo(mentor)
//        mail.setCc(mentee)
//        mail.setFrom('FHTMentoringProgramme@gmail.com')
//        mail.setSubject('Greetings from Mentoring Programme...')
//        mail.setText(body)
//
//        javaMailSender.send(mail)

        MimeMessagePreparator preparator = new MimeMessagePreparator() {
            void prepare(MimeMessage mimeMessage) throws Exception {
                mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(mentor))
                mimeMessage.setRecipient(Message.RecipientType.CC, new InternetAddress(mentee))
                mimeMessage.setFrom(new InternetAddress("FHTMentoringProgramme@gmail.com", 'Assurity Mentoring Programme'))
                mimeMessage.setSubject('Greetings from Mentoring Programme...')
                mimeMessage.setText(body)
            }
        }
        javaMailSender.send(preparator)
    }
}
