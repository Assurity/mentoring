package nz.co.assurity.mentoring.Services

import com.google.cloud.datastore.*
import nz.co.assurity.mentoring.entities.Category
import nz.co.assurity.mentoring.entities.Match
import nz.co.assurity.mentoring.entities.Skill
import nz.co.assurity.mentoring.entities.Staff
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mail.MailException
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

@Service
class MentoringService {

    private final Logger log = LoggerFactory.getLogger(MentoringService.class);

    @Autowired
    Datastore datastore;

    @Autowired
    NotificationService notificationService;

    private KeyFactory staffKeyFactory
    private KeyFactory skillKeyFactory
    private KeyFactory categoryKeyFactory
    private KeyFactory matchKeyFactory

    @PostConstruct
    void initializeKeyFactories() {
        log.info("Initializing key factories");
        staffKeyFactory = datastore.newKeyFactory().setKind("staff")
        skillKeyFactory = datastore.newKeyFactory().setKind("skill")
        categoryKeyFactory = datastore.newKeyFactory().setKind("category")
        matchKeyFactory = datastore.newKeyFactory().setKind("match")
    }

    Staff getStaff(String id) {
        Query<Entity> query = Query.newEntityQueryBuilder().setKind("staff").setFilter(StructuredQuery.PropertyFilter.eq("__key__", staffKeyFactory.newKey(id))).build();

        Entity staff
        Iterator<Entity> tasks = datastore.run(query)
        while (tasks.hasNext()) {
            Entity task = tasks.next();
            staff = task
        }

        return new Staff(staff)
    }

    Staff[] getAllStaff() {
        Query<Entity> query = Query.newEntityQueryBuilder().setKind("staff").build();

        List<Staff> staffList = []
        Iterator<Entity> results = datastore.run(query)

        while (results.hasNext()) {
            Entity result = results.next();
            staffList.add(new Staff(result))
        }

        return staffList
    }

    Skill[] getAllSkills() {
        Query<Entity> query = Query.newEntityQueryBuilder().setKind("skill").build();

        List<Skill> skillList = []
        Iterator<Entity> results = datastore.run(query)
        while (results.hasNext()) {
            Entity result = results.next();
            skillList.add(new Skill(result))
        }

        return skillList
    }

    Category[] getAllCategories() {
        Query<Entity> query = Query.newEntityQueryBuilder().setKind("category").build();

        List<Category> categoryList = []
        Iterator<Entity> results = datastore.run(query)
        while (results.hasNext()) {
            Entity result = results.next();
            categoryList.add(new Category(result))
        }

        return categoryList
    }

    Match[] getAllMatch(String name) {
        Query<Entity> query = Query.newEntityQueryBuilder().setKind("match").setFilter(StructuredQuery.PropertyFilter.eq("mentee", name)).build();

        List<Match> matchList = []
        Iterator<Entity> results = datastore.run(query)
        while (results.hasNext()) {
            Entity result = results.next();
            matchList.add(new Match(result))
        }

        return matchList
    }

    def upsertCategory(Category category, String id) {
        Key key = categoryKeyFactory.newKey(id)
        Entity categoryEntity = datastore.get(key)
        if (categoryEntity) {
            categoryEntity = Entity.newBuilder(categoryEntity)
                .set("name", category.name)
                .set("description", category.description)
                .build()
        } else {
            categoryEntity = Entity.newBuilder(key)
                .set("name", category.name)
                .set("description", category.description)
                .build()
        }
        datastore.put(categoryEntity)
    }

    def upsertStaff(Staff staff, String id) {
        List<StringValue> mentorSkills = []
        List<StringValue> menteeSkills = []

        for(item in staff.mentorSkills) {
            mentorSkills.add(new StringValue(item))
        }

        for(item in staff.menteeSkills) {
            menteeSkills.add(new StringValue(item))
        }

        Key key = staffKeyFactory.newKey(id)
        Entity staffEntity = Entity.newBuilder(key)
                .set('email', staff.getEmail())
                .set("menteeSkills", menteeSkills)
                .set("mentorSkills", mentorSkills)
                .set('name', staff.getName())
                .build()
        datastore.put(staffEntity)
    }

    def upsertStaffSkills(Staff staff, String id) {
        List<StringValue> mentorSkills = []
        List<StringValue> menteeSkills = []

        for(item in staff.mentorSkills) {
            mentorSkills.add(new StringValue(item))
        }

        for(item in staff.menteeSkills) {
            menteeSkills.add(new StringValue(item))
        }

        Key key = staffKeyFactory.newKey(id)
        Entity staffEntity = Entity.newBuilder(datastore.get(key))
                .set("menteeSkills", menteeSkills)
                .set("mentorSkills", mentorSkills)
                .build()
        datastore.put(staffEntity)
    }

    def upsertMatch(Match match) {
        String id = match.mentor + match.mentee + match.skill
        id = id.trim().toLowerCase().replace(' ', '')

        Key key = matchKeyFactory.newKey(id)

        Entity matchEntity = datastore.get(key)
        if (matchEntity) {
            matchEntity = Entity.newBuilder(matchEntity)
                    .set("mentor", match.mentor)
                    .set("mentee", match.mentee)
                    .set('skill', match.skill)
                    .build()
        } else {
            matchEntity = Entity.newBuilder(key)
                    .set("mentor", match.mentor)
                    .set("mentee", match.mentee)
                    .set('skill', match.skill)
                    .build()
        }

        datastore.put(matchEntity)
    }

    def sendEmail(Match match) {
        String mentorAddress = match.mentor
        String menteeAddress = match.mentee
        String emailBody = 'Hi ' + match.mentor + ', ' + match.mentee + ' selected you as mentor to teach ' + match.skill
        mentorAddress = mentorAddress.trim().toLowerCase().replace(' ', '.') + '@assurity.co.nz'
        menteeAddress = menteeAddress.trim().toLowerCase().replace(' ', '.') + '@assurity.co.nz'

        try {
            notificationService.sendNotification(mentorAddress, menteeAddress, emailBody)
        } catch (MailException e) {
            log.info('Send email error: ' + e.getMessage())
        }
    }

    def deleteMatch(mentor, mentee, skill) {
        String id = mentor + mentee + skill
        id = id.trim().toLowerCase().replace(' ', '')
        println 'Delete Match key: ' + id

        Key key = matchKeyFactory.newKey(id);
        datastore.delete(key);
    }

//    Entity createStaff(Staff staff) {
//        return datastore.put(upsertStaff(staff));
//    }

    void deleteStaff(Staff staff) {

    }

//    private Entity upsertStaff(Staff staff) {
//        Key key = staffKeyFactory.newKey(staff.getId())
//        println 'skills are...........'
//        println staff.menteeSkills
//        println staff.mentorSkills
//        return Entity.newBuilder(key)
//                .set("email", staff.getEmail())
//                .set("name", staff.getName())
//                .set("mentorSkills", "1", "2")
//                .set("menteeSkills", "3", "4")
//                .build()
//    }
}