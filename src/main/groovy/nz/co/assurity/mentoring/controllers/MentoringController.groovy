package nz.co.assurity.mentoring.controllers

import com.google.cloud.datastore.DatastoreException
import nz.co.assurity.mentoring.Services.MentoringService
import nz.co.assurity.mentoring.entities.Category
import nz.co.assurity.mentoring.entities.Match
import nz.co.assurity.mentoring.entities.Message
import nz.co.assurity.mentoring.entities.Staff
import org.springframework.http.ResponseEntity
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

import javax.validation.Valid


@RestController
class MentoringController {

    @Autowired
    MentoringService staffService

    /**
     * Get one staff info via id
     *
     * @param id
     * @return staff
     */
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/staff/{id}", method = RequestMethod.GET)
    getStaff(@PathVariable String id) {
        println 'getStaff: id - ' + id
        staffService.getStaff(id)
    }

    /**
     * Upsert a new staff
     *
     * No need for a staff ID for now when doing a Upsert action
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/newStaff/{id}", method = RequestMethod.POST)
    ResponseEntity<Message> addStaff(@Valid @RequestBody Staff staff, @PathVariable id) {
        println 'addStaff: staff - ' + staff + '; id - ' + id
        try {
            staffService.upsertStaff(staff, id)
        } catch (DatastoreException e) {
            return ResponseEntity.ok().body(new Message('failed to add a new staff'))
        }
        return ResponseEntity.ok().body(new Message('add a new staff'));
    }

    /**
     * Get all staff information
     *
     * @return an array of staff
     */
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/allStaff", method = RequestMethod.GET)
    getAllStaff() {
        println 'getAllStaff: '
        staffService.getAllStaff()
    }

//    /**
//     * Delete a staff info
//     *
//     * @param id
//     * @return
//     */
//    @RequestMapping(value = "/staff/{id}", method = RequestMethod.DELETE)
//    ResponseEntity<Message> deleteStaff(@PathVariable String id) {
//        println 'deleting staff, id is: ' + id
//    }

    /**
     * Get a list with all skills info
     *
     * @return an array of skill
     */
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/skills", method = RequestMethod.GET)
    getAllSkills() {
        println 'getAllSkills: '
        staffService.getAllSkills()
    }

    /**
     * Update and insert mentor skills for a specific mentee
     *
     * @return none
     */
    @RequestMapping(value = "/staffSkillsUpdate/{id}", method = RequestMethod.POST)
    ResponseEntity<Message> upsertStaffSkills(@RequestBody Staff staff, @PathVariable String id) {
        println 'upsertStaffSkills: staff - ' + staff + '; id - ' + id
        try {
            staffService.upsertStaffSkills(staff, id)
        } catch (DatastoreException e) {
            return ResponseEntity.ok().body(new Message('staff skills failed to update'))
        }
        return ResponseEntity.ok().body(new Message('staff skills updated'))
    }

    /**
     * Get a list with all matches info via user name
     *
     * @return an array of match
     */
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/allMatch/{name}", method = RequestMethod.GET)
    getAllMatches(@PathVariable name) {
        println 'getAllMatches: name - ' + name
        staffService.getAllMatch(name)
    }

//    /**
//     * Upsert skill info
//     *
//     * @return
//     */
//    @RequestMapping(value = "/skill", method = RequestMethod.POST)
//    String updateSkill(@Valid @RequestBody Skill skill) {
//        String str = 'a skill info is updated.....'
//        return str
//    }

    /**
     * Update and insert match info
     *
     * @return none
     */
    @RequestMapping(value = "/match", method = RequestMethod.POST)
    ResponseEntity<Message> upsertMatchInfo(@RequestBody Match match) {
        println 'upsertMatchInfo: match - ' + match
        try {
            staffService.upsertMatch(match)
            staffService.sendEmail(match)
        } catch (DatastoreException e) {
            return ResponseEntity.ok().body(new Message('match info failed to update'))
        }
        return ResponseEntity.ok().body(new Message('match info updated'))
    }

    /**
     * Delete match info
     *
     * @return none
     */
    @RequestMapping(value = "/deleteMatch/{mentor}/{mentee}/{skill}", method = RequestMethod.DELETE)
    ResponseEntity<Message> deleteMatchInfo(@PathVariable mentor, @PathVariable mentee, @PathVariable skill) {
        println 'deleteMatchInfo: mentor - ' + mentor + '; mentee - ' + mentee + '; skill - ' + skill
        try {
            staffService.deleteMatch(mentor, mentee, skill)
        } catch (DatastoreException e) {
            return ResponseEntity.ok().body(new Message('match info failed to delete'))
        }
        return ResponseEntity.ok().body(new Message('match info delete'))
    }

    /**
     * Get a list with all categories info
     *
     * @return an array of all categories
     */
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    getAllCategories() {
        println 'getAllCategories'
        staffService.getAllCategories()
    }

    /**
     * Upsert category info
     *
     * @return none
     */
    @RequestMapping(value = "/category/{id}", method = RequestMethod.POST)
    ResponseEntity<Message> upsertCategory(@RequestBody Category category, @PathVariable String id) {
        println 'upsertCategory: category - ' + category + '; id - ' + id
        try {
            staffService.upsertCategory(category, id)
        } catch (DatastoreException e) {
            return ResponseEntity.ok().body(new Message('failed to upsert category'))
        }
        return ResponseEntity.ok().body(new Message('upsert category'))
    }
}